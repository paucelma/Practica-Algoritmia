#include "OpenAddressHashTable.hh"

OpenAddressHashTable::OpenAddressHashTable(){}

OpenAddressHashTable::OpenAddressHashTable(unsigned int n_keys){
	long size = 2*nextpow2(n_keys);
	cout << "Size: " << size << endl;
	table = vector<pair<int,int>>(size,make_pair(0,0));
	deleteMe = vector<bool>(size,false);
	isAssigned = vector<bool>(size,false);
	srand(time(NULL));
	a1 = 0.6180339887; //Knuth suggest this value is likely to work well in the multiplication method
	//a2 = rand() /dd double(RAND_MAX);
}

unsigned int OpenAddressHashTable::nextpow2(unsigned int n){
	n--;
	n |= n>>1;
	n |= n>>2;
	n |= n>>4;
	n |= n>>8;
	n |= n>>16;
	n++;
	return n;
}

//Multiplication hashing
unsigned int OpenAddressHashTable::mult_hash(int k, double a){
	double intPart;
	double aux = modf(a*k,&intPart);
	return floor(getSize()*aux);
}

unsigned int OpenAddressHashTable::div_hash(int k){
	return (k%getSize())|1;
}

unsigned int OpenAddressHashTable::probe(int k, unsigned int i){
	return (mult_hash(k,a1) + i*div_hash(k))%getSize();
}

unsigned int OpenAddressHashTable::getSize(){
	return table.size();
}


bool OpenAddressHashTable::search(int k){
	for(unsigned int i = 0; i < getSize(); ++i){
		unsigned int index = probe(k,i);
		if(!isAssigned[index]) return false;
		else if(table[index].first == k){
			return !deleteMe[index];
		}
	}
}

int OpenAddressHashTable::get_value(int k){
	for(unsigned int i = 0; i < getSize(); ++i){
		unsigned int index = probe(k,i);
		if(table[index].first == k && !deleteMe[index]){
			return table[index].second;	
		}
	}
	return 0;
}

void OpenAddressHashTable::insert(int k, int v){
	bool inserted = false;
	for(unsigned int i = 0; i < getSize() && !inserted; ++i){
		unsigned int index = probe(k,i);
		if(deleteMe[index] || !isAssigned[index] || table[index].first == k){
			table[index] = make_pair(k,v);
			isAssigned[index] = true;
			deleteMe[index] = false;
			inserted = true;
		}
	}	
}

void OpenAddressHashTable::remove(int k){
	bool removed = false;
	for(unsigned int i = 0; i < getSize() && !removed;++i){
		unsigned int index = probe(k,i);
		if(isAssigned[index] && !deleteMe[index] && table[index].first == k){
			deleteMe[index] = true;
			removed = true;
		}
	}	
}
