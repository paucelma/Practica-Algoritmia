#include "OpenAddressHashTable.hh"

int main(){
	unsigned int n;
	cin >> n;
	OpenAddressHashTable h = OpenAddressHashTable(n);
	srand(time(NULL));
	vector<int> v = vector<int>(n);
	for(unsigned int i = 0; i < n; ++i){
		v[i] = rand();
	}
	for(unsigned int i = 0; i < n; ++i){
		h.insert(v[i],i);
	}
	cout << "Inserted stuff" << endl;
	for(unsigned int i = 0; i < n; ++i){
		cout << "Search " << v[i] <<": " << h.search(v[i]) << endl;
	}
	for(unsigned int i = 0; i < n; ++i){
		cout << "Get " << v[i] << ": " << h.get_value(v[i]) << endl;
	}
	for(unsigned int i = n/2; i < n; ++i){
		h.remove(v[i]);
	}
	cout << "Removed stuff" << endl;
	for(unsigned int i = 0; i < n; ++i){
		cout << "Search " << v[i] << ": " << h.search(v[i]) <<  endl;
	}
	for(unsigned int i = n/2; i < n; ++i){
		h.insert(v[i],-i);
	}
	cout << "Inserted stuff" << endl;
	for(unsigned int i = 0; i < n; ++i){
		cout << "Search " << v[i] << ": " << h.search(v[i]) << endl;
	}
	for(unsigned int i = 0; i < n; ++i){
		cout << "Get " << v[i] << ": " << h.get_value(v[i]) << endl;
	}
}
