#include "AVL.h"
#include <iostream>
#include <list>
#include <iterator>
#include <cmath>
using namespace std;


AVL::AVL() {
    root = nullptr;
}

void AVL::updateHeight(Node* n) {
    n->height = 1 + max(getHeight(n->left), getHeight(n->right));
}

int AVL::getHeight(Node* n) {
    if (n == nullptr) return -1;
    else return n->height;
}

int AVL::getMaxHeight() {
    if (root == nullptr) return 0;
    return getHeight(root) + 1;
}

void AVL::insert(int key) {
    insert(root, key);
}
    
void AVL::insert(Node* &n, int key) {
    //cout << "INSERT" << endl;
    if (n == nullptr) {
        n = new Node(key, nullptr, nullptr, 0);
        //cout << "NEW NODE" << endl;
    } else {
        if (key < n->key) {
            //cout << "IM LOWER" << endl;
            insert(n->left, key);
            if (getHeight(n->left) - getHeight(n->right) == 2) {
                if (key < n->left->key) rotateLL(n);
                else {
                    rotateRR(n->left);
                    rotateLL(n);
                }
            }
            this->updateHeight(n);
        }
        else if (key > n->key) {
            //cout << "IM HIGHER" << endl;
            insert(n->right, key);
            if (getHeight(n->right) - getHeight(n->left) == 2) {
                if (key > n->right->key) rotateRR(n);
                else {
                    rotateLL(n->right);
                    rotateRR(n);
                }
            }
            this->updateHeight(n);
        }
    }
}


bool AVL::isEmpty() {
    return (root == nullptr);
}

// CHECK IF KEY IN HASHTABLE
bool AVL::search(int key) {
    if (isEmpty()) return false;
    else search(root, key);
}

bool AVL::search(Node* n, int key) {
    if (n == nullptr) return false;
    if (key < n->key) return search(n->left, key);
    else if (key > n->key) return search(n->right, key);
    else return (key == n->key);
}

// DELETE
void AVL::remove(int key) {};


void AVL::rotateRR(Node* &n) {
    Node *tmp = n;
    n = n->right;
    tmp->right = n->left;
    n->left = tmp;
    updateHeight(tmp);
    updateHeight(n);
}

void AVL::rotateLL(Node* &n) {
    Node *tmp = n;
    n = n->left;
    tmp->left = n->right;
    n->right = tmp;
    updateHeight(tmp);
    updateHeight(n);
}

void AVL::inorder() {
    if (!isEmpty()) inorder(root);
    cout << endl;
}

void AVL::inorder(Node* n) {
    if (n != nullptr) {
        inorder(n->left);
        cout << n->key << " ";
        inorder(n->right);
    }
}


