#include "AVLHashTable.h"

AVLHashTable::AVLHashTable(){}

AVLHashTable::AVLHashTable(unsigned int b) {
    buckets = nextPrime(b);
    table = new AVL[buckets];

    // Analisis
    collisions = 0;
    accesses = 0;
    hash = 0;
}

AVLHashTable::AVLHashTable(unsigned int b, unsigned int hashFunc) {
    buckets = (hashFunc == 0) ? nextPrime(b) : b;
    table = new AVL[buckets];
    collisions = 0;
    accesses = 0;
    hash = hashFunc;
}

void AVLHashTable::insert(int key) { 
    unsigned int index = hashFunction(key); 
    table[index].insert(key);  
}

bool AVLHashTable::search(int key) {
	unsigned int index = hashFunction(key); 
    return table[index].search(key);  
}

void AVLHashTable::remove(int key) {
}


void AVLHashTable::displayHash() {     
} 

void AVLHashTable::displayResults() {
}

void AVLHashTable::memoryUsage() {
}


unsigned int AVLHashTable::hashFunction(int x) {
	unsigned int index;
	if (hash == 0) return divHashFunction(x);
	return multHashFunction(x);
}

unsigned int AVLHashTable::multHashFunction(int x) {
	double intPart;
	double aux = modf(0.61803398 * x, &intPart);
	return floor(buckets * aux);
}

unsigned int AVLHashTable::divHashFunction(int x) {
    return (x % buckets); 
}

unsigned int AVLHashTable::size() {
    return buckets;
}

unsigned int AVLHashTable::nextPrime(unsigned int n) {
	auto isPrime = [](unsigned int n) {
		if (n < 2) return false;
		if (n < 4) return true;
		if (n%2 == 0 || n%3 == 0) return false;
		for (int i=5; i*i<n; i+=6) {
			if (n%i == 0 || n%(i+2) == 0) return false;
		}
		return true;
	};

	n++;

	while (!isPrime(n)) n++;

	return n;
} 

unsigned int AVLHashTable::nextpow2(unsigned int n) {
	n--;
	n |= n>>1;
	n |= n>>2;
	n |= n>>4;
	n |= n>>8;
	n |= n>>16;
	n++;
	return n;
} 


float AVLHashTable::getCollisionPercentatge() {
	if (accesses == 0) return 0.0;
	return float(collisions) / float(accesses);
}

int AVLHashTable::getMaxConflict() {
	int max = 0;
	for (int i = 0; i < buckets; ++i)
	{	
		int s = table[i].getMaxHeight();
		if (s > max) {
			max = s;
		}
	}
	return max;
}