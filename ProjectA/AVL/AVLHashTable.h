#include "AVL.h"
#include <iostream>
#include <list>
#include <iterator>
#include <cmath>
using namespace std;

class AVLHashTable
{
    private:
    unsigned int buckets;
    unsigned int prime;
    unsigned int hash;    
    unsigned int accesses = 0;
    unsigned int collisions = 0;
    AVL *table; 

    public:
    // INITIALIZE
    AVLHashTable();

    AVLHashTable(unsigned int b);

    AVLHashTable(unsigned int b, unsigned int hash);
  
    // INSERTION
    void insert(int key);
    
    // CHECK IF KEY IN HASHTABLE
    bool search(int key);


    // DELETE
    void remove(int key);
    
 
    // PRINTS CONTENTS OF DICTIONARI
    void displayHash();
    void displayResults();
    void memoryUsage();
    
    // HASH FUCNTION
    unsigned int hashFunction(int x);

    unsigned int divHashFunction(int x);

    unsigned int multHashFunction(int x);
    
    unsigned int size();

    unsigned int nextPrime(unsigned int n);

    unsigned int nextpow2(unsigned int n);

    float getCollisionPercentatge();

    int getMaxConflict();
}; 