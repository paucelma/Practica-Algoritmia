#include <iostream>
#include <list>
#include <iterator>
#include <cmath>
using namespace std;

#ifndef Node_H
#define Node_H

struct Node
{
    int key;
    Node* left;
    Node* right;
    int height;

    Node(int k, Node* l, Node* r, int h) {
        key = k;
        left = l;
        right = r;
        height = h;
    }
};

#endif /* Node_H */

class AVL
{
    private:
    Node* root;

    public:
    AVL();

    void updateHeight(Node* n);
    int getHeight(Node* n);
    int getMaxHeight();
    bool isEmpty();
    // INSERTION
    void insert(int key);
    void insert(Node* &n, int key);
    
    // CHECK IF KEY IN HASHTABLE
    bool search(int key);
    bool search(Node* n, int key);

    // DELETE
    void remove(int key);
    void rotateRR(Node* &n);
    void rotateLL(Node* &n);

    void inorder();
    void inorder(Node* n);

}; 