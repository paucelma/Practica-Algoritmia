#include <iostream> 
#include <fstream>
#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
	ofstream file1, file2, file3;
	file1.open("arxiu1.txt");
	file2.open("arxiu2.txt");
	srand(time(NULL));

	unsigned int n;
	cin >> n;

	vector<bool> selected = vector<bool>(100000, false);
	vector<unsigned int> entradesArxiu2;

	unsigned int random;
	for (int i = 0; i < n; ++i) {
		random = rand() % 100000;

		while (selected[random]) random = rand() % 100000;
		selected[random] = true;

		file1 << random << ' ';
		entradesArxiu2.push_back(random);
		if (i!=0 and i%10 == 0) file1 << '\n';
	}

	for (int i = 0; i < 2*n; ++i) {
		random = rand() % 100000;

		while (selected[random]) random = rand() % 100000;
		selected[random] = true;

		entradesArxiu2.push_back(random);
	}

	std::random_shuffle(entradesArxiu2.begin(), entradesArxiu2.end());

	for (int i = 0; i < entradesArxiu2.size(); i++) {
		file2 << entradesArxiu2[i] << ' ';
		if (i!=0 and i%10 == 0) file2 << '\n';
	}
 
}