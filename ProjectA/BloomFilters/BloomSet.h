#pragma once
#include <vector>
using namespace std;
class BloomSet {
public:
	BloomSet(int n);
	BloomSet(int n, int k);
	bool find(int n);
	int put(int n);
	int size();
	int emptyPositions();
private:
	int EP; //empty postions
	bool prime(int n);
	int hash(int hashn, int n);
	vector <bool> bitvector;
	vector <int> primes;
};