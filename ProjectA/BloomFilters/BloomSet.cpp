#include "BloomSet.h"

BloomSet::BloomSet(int n) {
	int m = n*3.5; int k = 2;
	if (m % 2 == 0) ++m;
	for (; !prime(m); m += 2);
	*this = BloomSet(m, k);
}

BloomSet::BloomSet(int m, int k) {
	primes = vector<int>(k);
	bitvector = vector<bool>(m);
	EP = m;
	if (m % 2 == 0) --m;
	else m -= 2;
	for (int i = 0; i < primes.size() && m > 2; m -= 2) {
		if (prime(m)) {
			primes[i] = m;
			++i;
		}
	}
}

bool BloomSet::find(int n) {
	for (int i = 0; i < primes.size(); ++i) if (!bitvector[hash(i, n)]) return false;
	return true;
}

int BloomSet::put(int n) {
	int returnvalue = 0;
	for (int i = 0; i < primes.size(); ++i) {
		if (!bitvector[hash(i, n)]) ++returnvalue;
		bitvector[hash(i, n)] = true;
	}
	EP -= returnvalue;
	return returnvalue;
}

int BloomSet::hash(int hashn, int n) {
	return (n * primes[hashn]) % bitvector.size();
}

bool BloomSet::prime(int n) {
	if (n % 2 == 0) return false;
	for (int i = 3; i*i < n; i += 2) { if (n%i == 0) return false; }
	return true;
}

int BloomSet::size() {
	return bitvector.size();
}

int BloomSet::emptyPositions() {
	return EP;
}