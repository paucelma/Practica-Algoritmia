#include <iostream>
#include "BloomSet.h"
#include <fstream>

using namespace std;

int main() {
	ifstream in;
	int n;
	ofstream out;
	if (false) {
		out.open("EmptyPositions.txt");
		in.open("../Fitxers/arxiu1.txt");
		BloomSet BS(1000);
		while (in >> n) {
			out << BS.emptyPositions() << endl;
			BS.put(n);
		}
		out << BS.emptyPositions();
		out.close();
		in.close();
		in.open("../Fitxers/arxiu2.txt");
		int counter = 0;
		while (in >> n) {
			if (BS.find(n)) ++counter;
		}
		in.close();
		cout << "Counter: " << counter << endl;
	}
	else {
		out.open("FalsePositives.txt");
		for (int m = 3511; m < 15000; m += 2) {
			in.open("../Fitxers/arxiu1.txt");
			BloomSet BS(m, 2);
			while (in >> n) BS.put(n);
			in.close(); 
			in.open("../Fitxers/arxiu2.txt");
			int counter = 0;
			while (in >> n) if (BS.find(n)) ++counter;
			in.close();
			out << counter-1000 << endl;
		}
		out.close();
		cout << "DONE" << endl;
	}
	cin >> n; //Espera per finalitzar
}