#include <iostream>
#include <list>
#include <iterator>
#include <cmath>
using namespace std;

class SeparateChainingHashTable
{
    private:
    typedef pair<int, int> Pair;
    typedef typename list<Pair>::iterator iterat;

    unsigned int buckets;
    unsigned int prime;
    unsigned int hash;    
    unsigned int accesses = 0;
    unsigned int collisions = 0;
    list<Pair> *table; 

    public:
    // INITIALIZE
    SeparateChainingHashTable();

    SeparateChainingHashTable(unsigned int b);

    SeparateChainingHashTable(unsigned int b, unsigned int hash);
  
    // INSERTION
    void insert(int key, int value);
    
    // CHECK IF KEY IN HASHTABLE
    bool search(int key, int &q);
    
    // GET VALUE
    int get_value (int key);

    // DELETE
    void remove(int key);
    
    // FIND
    iterat find (int key, unsigned int hashValue);
    iterat find (int key, unsigned int hashValue, int &q);
 
    // PRINTS CONTENTS OF DICTIONARI
    void displayHash();

    // PRINT RESULTS
    void displayResults();
    void memoryUsage();
    
    // HASH FUCNTION
    unsigned int hashFunction(int x);

    unsigned int divHashFunction(int x);

    unsigned int multHashFunction(int x);
    
    unsigned int size();

    unsigned int nextPrime(unsigned int n);

    unsigned int nextpow2(unsigned int n);

    float getCollisionPercentatge();

    int getMaxConflict();
}; 