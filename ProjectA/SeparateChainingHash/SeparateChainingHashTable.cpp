#include "SeparateChainingHashTable.h"

typedef pair<int, int> Pair;
typedef typename list<Pair>::iterator iterat;

SeparateChainingHashTable::SeparateChainingHashTable(){}

SeparateChainingHashTable::SeparateChainingHashTable(unsigned int b) {
    buckets = nextPrime(b);
    table = new list<Pair>[buckets];

    // Analisis
    collisions = 0;
    accesses = 0;
    hash = 0;
}

SeparateChainingHashTable::SeparateChainingHashTable(unsigned int b, unsigned int hashFunc) {
    buckets = (hashFunc == 0) ? nextPrime(b) : nextpow2(b);
    table = new list<Pair>[buckets];
    collisions = 0;
    accesses = 0;
    hash = hashFunc;
}



void SeparateChainingHashTable::insert(int key, int value) { 
    unsigned int index = hashFunction(key); 
    table[index].push_back(Pair(key, value));  
}

bool SeparateChainingHashTable::search(int key, int &q) {
    unsigned int index = hashFunction(key); 
    auto it = find(key, index, q);
    return it != table[index].end();
}

int SeparateChainingHashTable::get_value (int key) {
    unsigned int index = hashFunction(key);
    auto it = find(key, index);
    if (it != table[index].end()) return it->second;
    else return -1;
}

void SeparateChainingHashTable::remove(int key) {
	unsigned int index = hashFunction(key);
	auto it = find(key, index);
	if (it != table[index].end()) table[index].erase(it);
}

iterat SeparateChainingHashTable::find (int key, unsigned int hashValue) {
	accesses++;
	if (table[hashValue].size() > 1) collisions++;    
    auto it = table[hashValue].begin();
    while (it != table[hashValue].end() and it->first != key) {
    	++it;
    }
    return it;
}

iterat SeparateChainingHashTable::find (int key, unsigned int hashValue, int &q) {
	accesses++;
	if (table[hashValue].size() > 1) collisions++;    
    auto it = table[hashValue].begin();
    while (it != table[hashValue].end() and it->first != key) {
    	++it;
    	++q;
    }
    return it;
}

void SeparateChainingHashTable::displayHash() { 
    for (int i = 0; i < buckets; ++i) { 
    	cout << i; 
    	for (auto it = table[i].begin(); it != table[i].end(); ++it) {
      		cout << " --> (" << it->first << ", " << it->second << ") " ;
    	}
    	cout << endl; 
    }    
} 

// not used
void SeparateChainingHashTable::displayResults() {
    cout << "In the execution of this program there has been:" << endl;
    cout << "   - " << accesses << " accesses" << endl;
    cout << "   - " << collisions << " collisions" << endl;
}

// not used
void SeparateChainingHashTable::memoryUsage() {
	unsigned int emptyBuckets = 0;
	unsigned int numberOfCollisionBuckets = 0;
	unsigned int totalCollisionPerBucket = 0;

	for (int i = 0; i < buckets; ++i) {
		if (table[i].empty()) emptyBuckets++;
		else if (table[i].size() > 1) {
			numberOfCollisionBuckets++;
			totalCollisionPerBucket+=table[i].size();
		}
	}

	cout << "The memory usage of this execution has been:" << endl;
	cout << "   - " << emptyBuckets << " empty buckets" << endl;
	cout << "   - " << float(emptyBuckets)/float(buckets) << " empty percentatge" << endl;
	cout << "   - " << numberOfCollisionBuckets << " buckets with collisions" << endl;
	if (numberOfCollisionBuckets > 0) cout << "   - " << float(totalCollisionPerBucket)/float(numberOfCollisionBuckets) << " average number of elements in collision bucket" << endl;
}


unsigned int SeparateChainingHashTable::hashFunction(int x) {
	unsigned int index;
	if (hash == 0) return divHashFunction(x);
	return multHashFunction(x);
}

unsigned int SeparateChainingHashTable::multHashFunction(int x) {
	double intPart;
	double aux = modf(0.61803398 * x, &intPart);
	return floor(buckets * aux);
}

unsigned int SeparateChainingHashTable::divHashFunction(int x) {
    return (x % buckets); 
}

unsigned int SeparateChainingHashTable::size() {
    return buckets;
}

unsigned int SeparateChainingHashTable::nextPrime(unsigned int n) {
	auto isPrime = [](unsigned int n) {
		if (n < 2) return false;
		if (n < 4) return true;
		if (n%2 == 0 || n%3 == 0) return false;
		for (int i=5; i*i<n; i+=6) {
			if (n%i == 0 || n%(i+2) == 0) return false;
		}
		return true;
	};

	n++;

	while (!isPrime(n)) n++;

	return n;
} 

unsigned int SeparateChainingHashTable::nextpow2(unsigned int n) {
	n--;
	n |= n>>1;
	n |= n>>2;
	n |= n>>4;
	n |= n>>8;
	n |= n>>16;
	n++;
	return n;
} 


float SeparateChainingHashTable::getCollisionPercentatge() {
	return float(collisions) / float(accesses);
}

int SeparateChainingHashTable::getMaxConflict() {
	int max = 0;
	for (int i = 0; i < buckets; ++i)
	{	
		int s = table[i].size();
		if (s > max) {
			max = s;
		}
	}
	return max;
}
