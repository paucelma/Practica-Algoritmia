#include "SeparateChainingHashTable.h"
#include <vector>
#include <fstream>
#include <string>


void insertsAndSearches(int n, int hash, const vector<int> &v1, const vector<int> &v2, int proba) {
	SeparateChainingHashTable h(n, hash);
    for (int i = 0; i < v1.size(); ++i) h.insert(v1[i], 0);
 
    bool b;
    int success = 0; 
    int fail = 0;
    int qS = 0;
    int qF = 0;
    int q;
    float alpha = 1001.00 / float(h.size());

    for (int i = 0; i < v2.size(); ++i) {
        q = 1;
    	b = h.search(v2[i], q);
    	if (b) {
            success++;
            qS+=q;
        }
    	else {
            fail++;
            qF+=q;
        }
    }

    if (proba == 0) cout << alpha << "  " << "Collision Percentage " <<h.getCollisionPercentatge() << endl;
    else if (proba == 1) cout << alpha << "  " << "Max accesses " << h.getMaxConflict() << endl;
    else if (proba == 2) cout << alpha << "  " << "Avg accesses in success " << float(qS)/float(success) << endl;
    else if (proba == 3) cout << alpha << "  " << "Avg accesses in fail " << float(qF)/float(fail) << endl;
}


int main() 
{ 	
	unsigned hash, n;

    cout << "Base size:" << endl;
    cin >> n;
    srand(time(NULL));

    ifstream arxiu1("../Fitxers/arxiu1.txt");
    if (!arxiu1) cout << "Couldn't open file" << endl;


    ifstream arxiu2("../Fitxers/arxiu2.txt");
    if (!arxiu2) cout << "Couldn't open file 2" << endl;

    vector<int> v1;
    vector<int> v2;
    unsigned int val;

    while(!arxiu1.eof()) {
    	arxiu1 >> val;
    	v1.push_back(val);
    }
    
    while(!arxiu2.eof()) {
    	arxiu2 >> val;
    	v2.push_back(val);
    }

    int input, mxCom;
    float cPer;
    cout << endl << "Menu: 0 collision percentage - 1 max collision - 2 success search test - 3 fail search test" << endl;
    cin >> input;
    while (input != -1) {
        cout << endl << "Hash function used: 0 div Hash - 1 mult Hash" << endl;
        cin >> hash;
    	for (float i = 0.05; i <= 0.95; i+=0.05) {
    		insertsAndSearches(floor(n / i), hash, v1, v2, input);		    
        }
        cout << endl << "Menu: 0 collision percentage - 1 max collision - 2 success search test - 3 fail search test" << endl;
    	cin >> input;
    }
}
