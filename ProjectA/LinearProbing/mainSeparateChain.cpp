#include "OpenAddressHashTable.h"
#include <vector>
#include <fstream>
#include <string>


void insertsAndSearches(int n, int type, const vector<int> &v1, const vector<int> &v2, float &cPer, int &mxClust) {
	OpenAddressHashTable h(n, type);

    for (int i = 0; i < n; ++i) h.insert(v1[i], 0);
 
    bool b;
    int success = 0; 
    int fail = 0;
    for (int i = 0; i < v2.size(); ++i) {
    	b = h.search(v2[i]);
    	if (b) success++;
    	else fail++;
    }

    cPer = h.getCollisionPercentatge();
    mxClust = h.maxClusterSize();
}


int main() 
{ 	
	unsigned int n, hash;
	cout << "Size of the hashTable" << endl;
	cin >> n;
    
    srand(time(NULL));

    ifstream arxiu1("arxiu1.txt");
    if (!arxiu1) cout << "Couldn't open file" << endl;


    ifstream arxiu2("arxiu2.txt");
    if (!arxiu2) cout << "Couldn't open file 2" << endl;

    vector<int> v1;
    vector<int> v2;
    unsigned int val;

    while(!arxiu1.eof()) {
    	arxiu1 >> val;
    	v1.push_back(val);
    }
    
    while(!arxiu2.eof()) {
    	arxiu2 >> val;
    	v2.push_back(val);
    }
    cout << "Size1: " << v1.size() << endl;
    cout << "Size2: " << v2.size() << endl;

    int input, mxCom;
    float cPer;
    cout << endl << "Menu: 0 collision percentage - 1 max collision" << endl;
    cin >> input;
    while (input != -1) {
        cout << endl << "Hash function used: 0 div Hash - 1 mult Hash" << endl;
        cin >> hash;
    	switch (input) {
    		case 0:
    			for (float alpha = 0.05; alpha <= 0.95; alpha+=0.05) {
    				insertsAndSearches(floor(n / alpha), hash, v1, v2, cPer, mxCom);
    				cout << cPer << endl;
    			}
    		case 1:
    			cin >> hash;
    			for (float alpha = 0.05; alpha <= 0.95; alpha+=0.05) {
    				insertsAndSearches(floor(n / alpha), hash, v1, v2, cPer, mxCom);
    				cout << alpha << " " << mxCom << endl;
    			}
    	}
        cout << endl << "Menu: 0 collision percentage - 1 max collision" << endl;
    	cin >> input;
    }
}
