#include "OpenAddressHashTable.hh"

int main(){
	int n;
	cin >> n;
	OpenAddressHashTable h = OpenAddressHashTable(10, n);
	
	h.insert(45);
	h.insert(13);
	h.insert(34);
	h.insert(67);
	h.insert(23);
	h.insert(74);

	h.displayTable();
	
	cout << "Max cluster size " << h.maxClusterSize() << endl;
	cout << "Collission count " << h.getCollissionCount() << endl;
}
