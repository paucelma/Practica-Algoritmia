#include "OpenAddressHashTable.hh"
#include <vector>
#include <fstream>
#include <string>


void insertsAndSearches(int max_keys, int n_keys, int type, const vector<int> &v1, const vector<int> &v2, float &cPer, int &mxClust, float &avgAccess) {
	OpenAddressHashTable h(max_keys, type);

    for (int i = 0; i < n_keys; ++i) h.insert(v1[i]);
    
    //cout << "GET SIZE " << h.getSize() << endl;

    bool b;
    int success = 0; 
    int fail = 0;
    int qS = 0;
    int q = 0;
    for (int i = 0; i < v2.size(); ++i) {
    	b = h.search(v2[i], q);
    	if (b) {
            qS += q;
            success++;
        }
    	else fail++;
    }	
    cPer = h.getCollissionPercentage();
    mxClust = h.maxClusterSize();
    avgAccess = float(qS) / float(success);
}


int main() 
{ 	
	unsigned int n, hash;
	cout << "Size of the hashTable" << endl;
	cin >> n;
    
    srand(time(NULL));

    ifstream arxiu1("arxiu1.txt");
    if (!arxiu1) cout << "Couldn't open file" << endl;


    ifstream arxiu2("arxiu2.txt");
    if (!arxiu2) cout << "Couldn't open file 2" << endl;

    vector<int> v1;
    vector<int> v2;
    unsigned int val;

    while(!arxiu1.eof()) {
    	arxiu1 >> val;
    	v1.push_back(val);
    }
    
    while(!arxiu2.eof()) {
    	arxiu2 >> val;
    	v2.push_back(val);
    }
    cout << "Size1: " << v1.size() << endl;
    cout << "Size2: " << v2.size() << endl;

    int input, mxCom;
    float cPer, avgAccess;
    cout << endl << "Menu: 0 collision percentage - 1 max collision - 2 avg access" << endl;
    cin >> input;
    while (input != -1) {
        cout << endl << "Hash function used: 0 Linear Probing - 1 Quadratic Probing - 2 Double Hashing" << endl;
        cin >> hash;
    	switch (input) {
    		case 0:
    			for (float alpha = 0.05; alpha <= 0.96; alpha+=0.05) {
    				insertsAndSearches(n,floor(n*alpha), hash, v1, v2, cPer, mxCom, avgAccess);
    				cout << alpha << " " << cPer << endl;
			}
			break;
    		case 1:
    			for (float alpha = 0.05; alpha <= 0.96; alpha+=0.05) {
    				insertsAndSearches(n, floor(n*alpha), hash, v1, v2, cPer, mxCom, avgAccess);
    				cout << alpha << " " << mxCom << endl;
    			}
			break;
		case 2: for(float alpha = 0.05; alpha <= 0.96; alpha+=0.05) {
				insertsAndSearches(n, floor(n*alpha), hash, v1, v2, cPer, mxCom, avgAccess);
				cout << avgAccess << endl;
			}
    	}
        cout << endl << "Menu: 0 collision percentage - 1 max collision - 2 avg accessg" << endl;
    	cin >> input;
    }
}
