using namespace std;

#include <utility>
#include <vector>
#include <cstdlib>
#include <time.h>
#include <iostream>
#include <cmath>

class OpenAddressHashTable {
private:
	unsigned int size;
	int type;
	unsigned int collissionCount;
	unsigned int totalAccess;
	vector<int> table;
	vector<bool> isAssigned;
	vector<bool> deleteMe;
	//Pre: n is any positive integer number
	//Post: The function returns the smallest power of 2 that is greater than n
	unsigned int nextpow2(unsigned int n);

	//Pre: k is any integer, a is a decimal number between 0 and 1, the size of the table is a power of 2
	//Post: The function computes an index in the table, a positive integer between 0 and size-1  
	unsigned int mult_hash(int k);

	//Pre: k is any integer
	//Post: The function computes an index in the table, a positive integer between 0 and size-1  
	unsigned int div_hash(int k);
	
	//Pre: k is any integer, i is any positive integer
	//Post: Computes an index in the table using double hashing, a positive integer between 0 and size-1
	unsigned int probe(int k, unsigned int i);	
public:
	//Pre:
	//Post: Returns the size of the hash table
	unsigned int getSize();

	int getType();

	void resetCollissionCount();

	float getCollissionPercentage();

	unsigned int getTotalAccess();

	unsigned int maxClusterSize();
	//Pre:
	//Post: Returns an instance of the OpenAddressHashTable class
	OpenAddressHashTable();
	
	//Pre: n_keys is any positive integer
	//Post: Returns an instance of the OpenAddressHashTable where:
	//table is of size enough to efficiently store n_keys objects
	//deleteMe is the same size of the table and all of it's elements equal false
	//isAssigned is the same size of the table and all of it's elements equal true
	//a1 is a suitable constant for the multiplication method
	OpenAddressHashTable(unsigned int n_keys);

	OpenAddressHashTable(unsigned int n_keys, int t);
	
	//Pre: k is any integer
	//Post: Returns whether or not (true/false) the key k is in the table
	bool search(int k);
	bool search(int k, int &q);

	//Pre: k and v are any integers
	//Post: Inserts the pair of (k,v) into the table
	void insert(int k);

	//Pre: k is any integer and search(k) is true
	//Post: Flags the pair with key k for deletion
	void remove(int k);

	void displayTable();

	unsigned int nextIndex(unsigned int i, int &h);
};
