#include "OpenAddressHashTable.hh"

OpenAddressHashTable::OpenAddressHashTable(){}

OpenAddressHashTable::OpenAddressHashTable(unsigned int n_keys){
	size = nextpow2(n_keys);
	type = 0;
	collissionCount = 0;
	totalAccess = 0;
	table = vector<int>(size, 0);
	isAssigned = vector<bool>(size, false);
	deleteMe = vector<bool>(size, false);
}

OpenAddressHashTable::OpenAddressHashTable(unsigned int n_keys, int t){
	size = nextpow2(n_keys);
	type = t;
	collissionCount = 0;
	totalAccess = 0;
	table = vector<int>(size, 0);
	isAssigned = vector<bool>(size, false);
	deleteMe = vector<bool>(size, false);
}


unsigned int OpenAddressHashTable::nextpow2(unsigned int n){
	n--;
	n |= n>>1;
	n |= n>>2;
	n |= n>>4;
	n |= n>>8;
	n |= n>>16;
	n++;
	return n;
}

//Multiplication hashing
unsigned int OpenAddressHashTable::mult_hash(int k){
	double intPart;
	double aux = modf(0.6180339887*k,&intPart);
	return floor(getSize()*aux);
}

unsigned int OpenAddressHashTable::div_hash(int k){
	return (k%getSize())|1;
}

unsigned int OpenAddressHashTable::probe(int k, unsigned int i){
	if (type == 0) {
		return (mult_hash(k) + i) % size;
	} if (type == 1) {
		return (mult_hash(k) + i*i) % size;
	}
	return (mult_hash(k) + i*div_hash(k))%getSize();
}

unsigned int OpenAddressHashTable::getSize(){
	return table.size();
}

void OpenAddressHashTable::resetCollissionCount(){
	collissionCount = 0;
	totalAccess = 0;
}

float OpenAddressHashTable::getCollissionPercentage(){
	return (collissionCount/(float)totalAccess)*100.0f;
}

unsigned int OpenAddressHashTable::getTotalAccess(){
	return totalAccess;
}

int OpenAddressHashTable::getType() {
	return type;
}

unsigned int OpenAddressHashTable::maxClusterSize(){
	int max = 0;
	int aux = max;
	for(int i = 0; i < getSize() || (isAssigned[i%getSize()] && aux <= getSize()); i++){
		if(!isAssigned[i%getSize()]){
			if(aux > max) max = aux;
			aux = 0;
		} else {
			aux++;
		}
	}
	if(aux > max) max = aux;
	return max;
}

bool OpenAddressHashTable::search(int k){
	for(unsigned int i = 0; i < getSize(); ++i){
		unsigned int index = probe(k,i);
		//++totalAccess;
		if(!isAssigned[index]) return false;
		else if(table[index] == k){
			return !deleteMe[index];
		}
		//++collissionCount;
	}
}

bool OpenAddressHashTable::search(int k, int &q){
	q = 0;
	for(unsigned int i = 0; i < getSize(); ++i){
		unsigned int index = probe(k,i);
		//++totalAccess;
		q++;
		if(!isAssigned[index]) return false;
		else if(table[index] == k){
			return !deleteMe[index];
		}
		//++collissionCount;
	}
}


void OpenAddressHashTable::insert(int k){
	bool inserted = false;
	for(unsigned int i = 0; i < getSize() && !inserted; ++i){
		unsigned int index = probe(k,i);
		//++totalAccess;
		if(deleteMe[index] || !isAssigned[index]){
			table[index] = k;
			isAssigned[index] = true;
			deleteMe[index] = false;
			inserted = true;	
		}
//		++collissionCount;
	}
}

void OpenAddressHashTable::remove(int k){
	bool removed = false;
	for(unsigned int i = 0; i < getSize() && !removed;++i){
		unsigned int index = probe(k,i);
		if(isAssigned[index] && !deleteMe[index] && table[index] == k){
			deleteMe[index] = true;
			removed = true;
		}
	}	
}

void OpenAddressHashTable::displayTable(){
	for (int i = 0; i < getSize(); ++i) {
		cout << i << "-> " << table[i] << endl;
	}
}

